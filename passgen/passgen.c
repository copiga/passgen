#include <stdio.h>

#include "copiga.h"

#ifdef matrixy
#define COLOURDEF 33
#define STDCOLOUR 0
#endif

int gen(int len,int quantity)
{
  int i, current = 0;
  int pass[len];
  srand(time(NULL)+getpid());
  while(current<quantity)
    {
      for(i = 1; i<=len;i++)
	{
	  pass[i]=(((rand())%100));
	  while(pass[i]<32||pass[i]>126)
	    {
	      pass[i]=(((rand())%100)+32);
	    }
#ifdef matrixy
	  printf ("\033[%dm%c", COLOURDEF, pass[i]);
#else
	  printf ("%c",pass[i]);
#endif
	}
      puts ("\r");
      current++;
    }
#ifdef matrixy
  printf("\033[%dm\n", STDCOLOUR);
#endif
}

int main(int argc, char *argv[])
{
  //printf("%d", argc);
  
  if(argc!=3)
    {
      int len, quantity, seed;
      puts ("please enter the length and quantity of passwords like\nlength:quantity:seed");
      scanf (" %d:%d", &len, &quantity);
      //  printf ("%d:%d", len, quantity);
      gen(len,quantity);
    } else 
    {
      /*printf ("%s,%s", argv[1], argv[2]);
      puts("passwords begin now");*/
      gen(atoi(argv[1]),atoi(argv[2]));
    }
}

