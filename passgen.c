#include <stdio.h>

#include "copiga.h"

int gen(int len,int quantity)
{
  int i, current = 0;
  int pass[len];
  while(current<quantity)
    {
      for(i = 1; i<=len;i++)
	{
	  pass[i]=(randgen(len%quantity)%100);
	  while(pass[i]<32||pass[i]>126)
	    {
	      pass[i]=(32+randgen(len%quantity+i)%100);
	    }
	  printf ("%c", pass[i]);
	}
      puts ("\r");
      current++;
    }
}

int main(int argc, char *argv[])
{
  printf("%d", argc);
  
  if(argc!=3)
    {
      int len, quantity;
      puts ("please enter the length and quantity of passwords like\nlength:quantity");
      scanf (" %d:%d", &len, &quantity);
      //  printf ("%d:%d", len, quantity);
      gen(len,quantity);
    } else 
    {
      /*printf ("%s,%s", argv[1], argv[2]);
      puts("passwords begin now");*/
      gen(atoi(argv[1]),atoi(argv[2]));
    }
}

